# FreeBSD Docker

An effort to create a docker system based on bhyve, similier to how it operates on Microsoft Windows.

## Project overview

In regards to this documentation, the 'client side' is always the FreeBSD host 
system as it is infact a client to the services offered by the docker service.

### Client Side

The following executables would have to be present, this is not an exhaustive 
list and is in reference to reaching the first milestone:

* docker - as exposed on the server, this will infact connect to docker-controller

    Target: 'docker run hello-world'
    Expected: The default greeting/test for docker

The actual outputs for this command will be a direct mirror of what is returned
by the the same command within the image.

The language this is developed in really has no implications at this stage 
the focus is really more on proving the system edge to edge.

### Server side

#### dockerd-controllert

#### create-image

### Image gen

Executed in order of the three numbers on the lhs.

* 000-execute.sh (one shot script to do all tasks, executed on host)
* 001-generate-base.sh (run from installation cd)
    Run from within a gentoo installation CD, this will fetch and build the 
    latest stage3, compile the kernel and install the base system onto an 
    attached disk.
* 100-generate-image.sh (run from within chroot)
    Remove any locales, man pages or other documentation
* 999-finalize.sh (run on host)

NOTE: Perhaps make this one massive script that takes ARG0 as which to execute?

Could be stored as seperate and smashed together, perhaps could use perl.
